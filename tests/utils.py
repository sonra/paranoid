import json
import os
from pprint import pprint

import paranoid
#import psutil
import xml.parsers.expat

script_directory = os.path.dirname(os.path.realpath(__file__))
output_dir = script_directory + '/out/'


def test_file_reading(input_file, final_output_dir, buffer_size, xpath=None):
    args = {
        'byteSize': buffer_size,
        'input': input_file,
        'outputDir': final_output_dir,
        'mask': xpath,
        'log': ''
    }
    print_memory_usage()
    print('Reading file [{}] with buffer size {}'.format(input_file, buffer_size))
    paranoid.commandLine(DictEx(args))
    file_to_test = os.path.basename(input_file)
    expected_output = os.path.sep.join([final_output_dir, file_to_test])
    assert os.path.exists(expected_output)
    # assert compare_files_line_by_line(input_file, expected_output)
    if file_to_test.endswith(".xml"):
        assert compare_xml_structure(input_file, expected_output)
    if file_to_test.endswith(".json"):
        assert compare_json_structure(input_file, expected_output)

    print_memory_usage()


def print_memory_usage():
    pass


def compare_files_line_by_line(input_file, expected_output):
    with open(input_file) as input_handler:
        with open(expected_output) as output_handler:
            line_number = 1
            input_line = input_handler.readline().strip()
            output_line = output_handler.readline().strip()
            while True:
                if not input_line and not output_line:
                    break

                if len(input_line) != len(output_line):
                    if len(input_line) != 0:
                        raise BaseException('Lines are not equal. Line number: {}'.format(line_number))

                input_line = input_handler.readline().strip()
                output_line = output_handler.readline().strip()
                line_number += 1
    return True


def compare_xml_structure(input_file, expected_output):
    source_structure = []
    target_structure = []

    def source_start_element(name, attrs):
        source_structure.append(['start', name, list(dict(attrs).keys())])

    def source_end_element(name):
        source_structure.append(['end', name])

    def target_start_element(name, attrs):
        target_structure.append(['start', name, list(dict(attrs).keys())])

    def target_end_element(name):
        target_structure.append(['end', name])

    with open(input_file) as input_handler:
        p = xml.parsers.expat.ParserCreate()
        p.StartElementHandler = source_start_element
        p.EndElementHandler = source_end_element
        p.Parse(input_handler.read(), 1)

    with open(expected_output) as output_handler:
        p = xml.parsers.expat.ParserCreate()
        p.StartElementHandler = target_start_element
        p.EndElementHandler = target_end_element
        p.Parse(output_handler.read(), 1)

    return source_structure == target_structure


def none_values(input):
    if isinstance(input, list):
        output = []
        for item in input:
            output.append(none_values(item))
        return output
    elif isinstance(input, dict):
        input
        output = {}
        for key in input.keys():
            if isinstance(input[key], dict) or isinstance(input[key], list):
                output[key] = none_values(input[key])
            else:
                output[key] = None
        return output
    else:
        return None


def compare_json_structure(input_file, expected_output):
    with open(input_file) as input_handler:
        input = json.load(input_handler)

    with open(expected_output) as output_handler:
        output = json.load(output_handler)

    source_structure = none_values(input)
    target_structure = none_values(output)
    return source_structure == target_structure


class DictEx(dict):
    def __getattr__(self, item):
        return self[item]

