import logging
import unittest

import dateutil
import dateutil.parser

from paranoid import validate_timestamp
from timestamp_validator_27 import validate_timestamp as validate_timestamp2


class TimeStampSuite(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(threadName)s - %(module)s - %(message)s')

        handler = logging.StreamHandler()
        handler.setFormatter(formatter)

        cls.logger = logging.getLogger('test')
        cls.logger.setLevel(logging.INFO)
        cls.logger.addHandler(handler)

    def test_timestamps(self):
        timestamps = [
            "Jun 09 2018 15:28:14"
            , "Apr 20 00:00:35 2010"
            , "2017-10-14T22:11:20+0000"
            , "2017-07-01T14:59:55.711Z"
            , "2017-08-19 12:17:55-0400"
            , "2018-02-27 15:35:20.311"
            , "2017-03-12 13:11:34.222-0700"
            , "10-04-19 12:00:17"
            , "06/01/22 04:11:05"
            , "23/Apr/2017:11:42:35"
            , "23/Apr/2017 11:42:35"
            , "23-Apr-2017 11:42:35"
            , "23-Apr-2017 11:42:35.883"
            , "23 Apr 2017 11:42:35"
        ]
        for ts in timestamps:
            with self.subTest(ts):
                r, f = validate_timestamp(ts)
                self.logger.info("Original [%s] -> Masked [%s]", ts, f)
                self.assertEqual(r, True)
                # validate masked timestamp
                f_ts = dateutil.parser.parse(f)
                f_ts_str = str(f_ts)
                self.assertIsNotNone(f_ts_str)
                self.logger.info("Masked Date - %s", f_ts_str)
                self.assertLess(f_ts.year, 2020)
                self.assertGreater(f_ts.year, 1910)

    def test_timestamps(self):
        timestamps = [
            "Jun 09 2018 15:28:14"
            , "Apr 20 00:00:35 2010"
            , "2017-10-14T22:11:20+0000"
            , "2017-07-01T14:59:55.711Z"
            , "2017-08-19 12:17:55-0400"
            , "2018-02-27 15:35:20.311"
            , "2017-03-12 13:11:34.222-0700"
            , "10-04-19 12:00:17"
            , "06/01/22 04:11:05"
            , "23/Apr/2017:11:42:35"
            , "23/Apr/2017 11:42:35"
            , "23-Apr-2017 11:42:35"
            , "23-Apr-2017 11:42:35.883"
            , "23 Apr 2017 11:42:35"
        ]
        for ts in timestamps:
            with self.subTest(ts):
                r, f = validate_timestamp2(ts)
                self.logger.info("Original [%s] -> Masked [%s]", ts, f)
                self.assertEqual(r, True)
                # validate masked timestamp
                f_ts = dateutil.parser.parse(f)
                f_ts_str = str(f_ts)
                self.assertIsNotNone(f_ts_str)
                self.logger.info("Masked Date - %s", f_ts_str)
                self.assertLess(f_ts.year, 2030)
                self.assertGreater(f_ts.year, 1910)


if __name__ == "__main__":
    unittest.main()
