import pprint
import time
import os
import paranoid
from datetime import datetime
from time import gmtime, strftime
from lxml import etree
import lxml
from io import StringIO
import logging
import json


class DictEx(dict):
    def __getattr__(self, item):
        return self[item]


# ---------------------------------------------------------------
# check file's type
# ---------------------------------------------------------------
def check_extension(_name, _ext):
    if not os.path.isfile(_name.path):
        return False

    _extension = _name.name.split('.')[-1]
    if _extension in _ext:
        return True

    return False


def flatten_dict(_ini_dict, separator='_', prefix=''):
    return {prefix + separator + k if prefix else k: v
            for kk, vv in _ini_dict.items()
            for k, v in flatten_dict(vv, separator, kk).items()
            } if isinstance(_ini_dict, dict) else {prefix:
                                                       _ini_dict}


from itertools import chain, starmap


def flatten_json(nested_json):
    """
        Flatten json object with nested keys into a single level.
        Args:
            nested_json: A nested json object.
        Returns:
            The flattened json object if successful, None otherwise.
    """
    if isinstance(nested_json, dict):
        out = dict()
    else:
        out = list()

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(nested_json)
    return out


def flatten_solution(_dict):
    """Flatten a nested json file"""

    def unpack(parent_key, parent_value):
        """Unpack one level of nesting in json file"""
        # Unpack one level only!!!

        if isinstance(parent_value, dict):
            for key, value in parent_value.items():
                temp1 = parent_key + '_' + key
                yield temp1, value
        elif isinstance(parent_value, list):
            i = 0
            for value in parent_value:
                temp2 = parent_key + '_' + str(i)
                i += 1
                yield temp2, value
        else:
            yield parent_key, parent_value

        # Keep iterating until the termination condition is satisfied

    while True:
        # Keep unpacking the json file until all values are atomic elements
        # (not dictionary or list)
        try:
            _dict = dict(chain.from_iterable(starmap(
                unpack, _dict.items())))
            if not any(isinstance(value, dict) for value in _dict.values(
            )) and not any(isinstance(value, list
                                      ) for value in _dict.values()):
                break
        except:
            break
    return _dict


# -----------------------------------------------------------------------
def test_json_compliance(_in, _out):
    logger = logging.getLogger()
    _out += _in.split('/')[-1]
    _err_count = 0
    try:
        with open(_in) as _in_f:
            _in_json = json.load(_in_f)
    except:
        logger.warning('Wrong JSON structute in the ' +
                       'input file, failed')
        return False
    try:
        with open(_out) as _out_f:
            _out_json = json.load(_out_f)
    except:
        logger.warning('Wrong JSON structute in the ' +
                       'output file, failed')
        return False

    _plain_in = flatten_solution(_in_json)
    # if plain json is list, transform it to dictionary
    if isinstance(_plain_in, list):
        _plain_in = {'AAA': _plain_in}

    # the same with output json
    _plain_out = flatten_solution(_out_json)
    if isinstance(_plain_out, list):
        _plain_out = {'AAA': _plain_out}

    _plain_counter = 0
    # to compare whether the keys remains the same as in origin
    _in_keys = _plain_in.keys()
    for _elem in _plain_out:
        if _elem not in _in_keys:
            logger.warning(f'Wrong key {_elem} in output file')
            _err_count += 1
            continue
        if type(_plain_out[_elem]) != type(_plain_in[_elem]
                    ) or (_plain_out[_elem] == _plain_in[_elem]
                    and not isinstance(_plain_in[_elem], bool) and
                    _plain_in[_elem] is not None
                    and isinstance(_plain_in[_elem], str) and
                    len(_plain_in[_elem]) > 2):
            logger.warning(
                f'Wrong masked {_elem}--->{_plain_in[_elem]}')
            _err_count += 1

    return _err_count

def spent_(_pr_time):
    _delta = time.time() - _pr_time
    if _delta >= 3600:
        _hours = int(_delta / 3600)
        _minutes = int((_delta - _hours * 3600) / 60)
        _seconds = _delta - _hours * 3600 - _minutes * 60
        return f'{_hours}h{_minutes}m{_seconds}s'
    if _delta > 60:
        _minutes = int(_delta / 60)
        _seconds = int(_delta - _minutes * 60)
        return f'{_minutes}m{_seconds}s'
    else:
        return f'{int(_delta)}s'


# -----------------------------------------------------------------------
def test_compliance(_in, _out, _mask):
    logger = logging.getLogger()

    # json check
    if _in.split('.')[-1] in 'json':
        return test_json_compliance(_in, _out)
    # xml check
    try:
        _tree_in = lxml.etree.parse(_in)
    except Exception as _error:
        logger.warning('Error while processing {}', format(_error))
        time.sleep(3)
        return 1
    _out_filename = _out + _in.split('/')[-1]
    try:
        _tree_out = lxml.etree.parse(_out_filename)
    except Exception as _error:
        logger.warning('Error while processing output file{}',
                       format(_error))
        time.sleep(3)
        return 1
    # find out all the child elements of xml either input file
    # and output one
    _in_els = _tree_in.xpath('.//*')
    _out_els = _tree_out.xpath('.//*')

    logger.info(f"Processing---{_in.split('/')[-1]}---")
    _log_errs = 0
    for _ii in range(len(_in_els)):

        if (_in_els[_ii].tag.strip()
                not in _out_els[_ii].tag.strip() or
                (_out_els[_ii].text is not None and
                 (len(_in_els[_ii].text.strip()) !=
                  len(_out_els[_ii].text.strip())
                  or (not _in_els[_ii].text.isdigit() and
                      len(_in_els[_ii].text.strip()) > 3 and
                      _in_els[_ii].text.strip() in
                      _out_els[_ii].text.strip())))
                ):
            _log_errs += 1
            logger.warning('Tag in  ---> ' + _in_els[_ii].tag)
            logger.warning('Text in ---> ' + _in_els[_ii].text)
            logger.warning('Tag out ---> ' + _out_els[_ii].tag)
            logger.warning('Text out---> ' + _out_els[_ii].text)
            _log_errs += 1


    # ------------------------------------------------------------------
    # Check xpath expression presence
    if len(_mask) > 3:
        _masks = _mask.split(',')
        for _cur_mask in _masks:
            _xpathes_in = _tree_in.xpath(_cur_mask)
            if len(_xpathes_in) == 0:
                logger.warning(" Wrong or not present xpath expression " +
                               "--->{}", format(_cur_mask))
            _xpathes_out = _tree_out.xpath(_cur_mask)
            _jj = 0
            for _xp in _xpathes_in:
                # check valid symbols presence
                if len(_xp.text.replace('\n', '').strip()) == 0:
                    continue
                if _xp.text in _xpathes_out[_jj].text:
                    logger.warning(" Wrong masking found " +
                                   "--->{}--->", format(_cur_mask) +
                                   "line {}", format(_xp.sourceline))
                    _log_errs += 1
                _jj += 1

    return _log_errs


# ------------------------------------------------------------------
def get_actual_xmls():
    _export_dir = os.getcwd() + '/tests/'
    with os.scandir(_export_dir +'out/') as _entries:
        _t_outs = [_entry.name for _entry in _entries
                   if check_extension(_entry, 'xml')]
    with os.scandir(_export_dir +'in/') as _entries:
        _t_ents = [_entry.path for _entry in _entries
                   if check_extension(_entry, 'xml') and
                   _entry.path.split('/')[-1] not in _t_outs]
    _total_entries = len(_t_ents)

    print('Total unchecked xml entries in the directory' +
            '-{}', format(_total_entries))
    time.sleep(2)

    return _t_ents


# ------------------------------------------------------------------
def get_actual_jsons():
    _export_dir = os.getcwd() + '/tests/'
    with os.scandir(_export_dir + 'out/') as _entries:
        _t_outs = [_entry.name for _entry in _entries
                   if check_extension(_entry, 'json')]
    with os.scandir(_export_dir +'in/') as _entries:
        _t_ents = [_entry.path for _entry in _entries
                   if check_extension(_entry, 'json') and
                   _entry.path.split('/')[-1] not in _t_outs]
    _total_entries = len(_t_ents)

    print(
        'Total unchecked json entries in the directory') # +
#        f'-{_total_entries}')
    time.sleep(2)

    return _t_ents

    #_t_ents = get_actual_xmls()
    #_t_ents += get_actual_jsons()  # len(_t_ents)
    # /home/sergey/Yandex.Disk/Sonra/paranoid/tests/test_compliance.py
# ------------------------------------------------------------------
def main():
    _err_total = 0
    _total_entries = 1
    _entry_counter = 0

    _t_ents = get_actual_xmls() + get_actual_jsons()

    for _entry in _t_ents:
        _args = {
            'byteSize': 10000,
            'input': _entry,
            'outputDir': os.getcwd() + '/tests/out/',
            'mask': '',
            'log': 'log_paranoid.txt'}
        try:
            paranoid.commandLine(DictEx(_args))
            # _err_total += test_compliance(_args['input'],
            #       _args['outputDir'], _args['mask'])

        except UnicodeDecodeError:
            print('Wrong file format, skiping...')
        _entry_counter += 1
        '''
        if _entry_counter % 250 == 0:
            print(f'{(_entry_counter / _total_entries) * 100:.3f}' +
                  '% completed')
    print(f' {_err_total} total errors found  ')
        '''


if __name__ == '__main__':
    main()
