import unittest
from tests.utils import test_file_reading
import os


class ParserTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # script_directory = os.path.dirname(os.path.realpath(__file__))
        cls.samples_directory = os.path.sep.join([ "in"])
        cls.output_directory = os.path.sep.join(['out'])
        cls.files_to_test = [os.path.sep.join([cls.samples_directory, x]) for x in os.listdir(cls.samples_directory)]

    def test100byte(self):
        test_output_directory = os.path.sep.join([self.output_directory, "100"])
        for file in self.files_to_test:
            if True:
                test_file_reading(input_file=file, buffer_size=100, final_output_dir=test_output_directory)

    def test1000bytes(self):
        test_output_directory = os.path.sep.join([self.output_directory, "1000"])
        for file in self.files_to_test:
            if True:
                test_file_reading(input_file=file, buffer_size=1000, final_output_dir=test_output_directory)

    def test10000bytes(self):
        test_output_directory = os.path.sep.join([self.output_directory, "10000"])
        for file in self.files_to_test:
            if True:
                test_file_reading(input_file=file, buffer_size=10000, final_output_dir=test_output_directory)

    def test100000bytes(self):
        test_output_directory = os.path.sep.join([self.output_directory, "100000"])
        for file in self.files_to_test:
            if True:
                test_file_reading(input_file=file, buffer_size=100000, final_output_dir=test_output_directory)

    def test1000000bytes(self):
        test_output_directory = os.path.sep.join([self.output_directory, "1000000"])
        for file in self.files_to_test:
            if True:
                test_file_reading(input_file=file, buffer_size=1000000, final_output_dir=test_output_directory)

    def test1000000bytesWithXpath(self):
        test_output_directory = os.path.sep.join([self.output_directory, "xpath_1000000"])
        for file in self.files_to_test:
            if True:
                test_file_reading(
                    input_file=file
                    , buffer_size=1000000
                    , final_output_dir=test_output_directory
                    , xpath="/items/item/name"
                )

    def test100000bytesWithXpath(self):
        test_output_directory = os.path.sep.join([self.output_directory, "xpath_100000"])
        for file in self.files_to_test:
            if True:
                test_file_reading(
                    input_file=file
                    , buffer_size=100000
                    , final_output_dir=test_output_directory
                    , xpath="/items/item/name"
                )

    def test10000bytesWithXpath(self):
        test_output_directory = os.path.sep.join([self.output_directory, "xpath_10000"])
        for file in self.files_to_test:
            if True:
                test_file_reading(
                    input_file=file
                    , buffer_size=10000
                    , final_output_dir=test_output_directory
                    , xpath="/items/item/name"
                )

    def test1000bytesWithXpath(self):
        test_output_directory = os.path.sep.join([self.output_directory, "xpath_1000"])
        for file in self.files_to_test:
            if True:
                test_file_reading(
                    input_file=file
                    , buffer_size=1000
                    , final_output_dir=test_output_directory
                    , xpath="/items/item/name"
                )

    def test100bytesWithXpath(self):
        test_output_directory = os.path.sep.join([self.output_directory, "xpath_100"])
        for file in self.files_to_test:
            if True:
                test_file_reading(
                    input_file=file
                    , buffer_size=100
                    , final_output_dir=test_output_directory
                    , xpath="/items/item/name"
                )


if __name__ == "__main__":
    unittest.main()
