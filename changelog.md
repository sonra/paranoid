# Change Log

## v1.2.5   22/09/2020
* FIX: fixed handling of self closing XML tags


## v1.2.3   [22/11/2019](https://bitbucket.org/sonra/paranoid/pull-requests/23/fix-nesting-of-sibling-tags-and-add/diff)
* NEW: Added masking by attributes XPath
* FIX: XML structure to avoid nesting of sibling tags

## v1.2.2   30/09/2019
* FIX: fixed multiple CDATA handling
* FIX: fixed timestamp dd/mm/yy
* FIX: fixed JSON entities masking

## v1.2.0   03/09/2019
* Modified: increased masking performance
* FIX: fixed CDATA and namespace handling
* FIX: fixed XML entities masking

## v1.1.5   26/07/2019
* FIX: excluded null values masking
* FIX: excluded escaped characters (new lines/tabs, etc) from masking
* FIX: keep masking text after commas

## v1.1.4   24/07/2019
* FIX: fixes for XML files partial reading
* FIX: keep boolean values as is, to preserve data types

## v1.1.3    10/07/2019

* FIX: fixes for XML files reading 

## v1.1.0    23/04/2019

* Added: -display tag masking statistics -logging 

## v1.1.0b0  8/04/2019

* Modified: masking tool to add changes for cdata

## v1.0.5  29/03/2019

* Added: -Readme.md -Change log -License